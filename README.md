# grafica

Repositorio con los elementos comunes de gráfica y comunicación visual para que todas tengamos acceso a nuestros logos, colores, etc...


## logotipo
en formato svg

![logo sindominio blanco y negro svg](svg/sd-logo-bn.svg "ese es el logo blanco y negro de sindominio")

![logo sindominio color svg](svg/sd-logo-color.svg "ese es el logo color de sindominio")

en fomato png

![logo sindominio png 196px blanco y negro](png/sd-logo-bn-196.png "ese es el logo blanco y negro de sindominio")

![logo sindominio 196px blanco](png/sd-logo-blanco-196.png"ese es el logo blanco y negro de sindominio")

![logo sindominio 196px color](png/sd-logo-color-196.png "ese es el logo blanco y negro de sindominio")

![logo sindominio blanco y negro 512px](png/sd-logo-bn-512.png "ese es el logo blanco y negro de sindominio")


## imagotipo
en formato svg

![imagotipo sindominio](svg/sd-nombre-sindominio.svg "ese es el nombre de sindominio")

en formato png

![imagotipo sindominio](png/sd-nombre-sindominio-500x40.png "ese es el nombre de sindominio")

tipografía del texto:

![Neuropolitical](fonts/neuropolitical.zip)  


## colores

* principal `#468698`
* principal oscuro `#325F6E`
* gris `#cccccc`


| nombre  | principal    | principal oscuro | gris          |
| ------- | ------------ | ---------------- | ------------- |
| hex #   | 468698       | 325F6E           | cccccc        |
| rgb     | (70,134,152) | (50,95,110)      | (204,204,204) |
|         |              |                  |               |
